const express = require("express");
const http = require("http");
const mysql = require("mysql2");
const socketIo = require("socket.io");
const path = require("path");
const mongoose = require("mongoose");
const app = express();
const server = http.createServer(app);
const io = socketIo(server);

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const pool = mysql.createPool({
  host: "127.0.0.1",
  user: "MaksPatsevko",
  password: "Maks1708",
  database: "internetprograming",
});

mongoose
  .connect(
    "mongodb+srv://maxumulian2109:euauAcL4cVTxiVd8@cluster0.mugfbja.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    console.log("Успішно підключено до бази даних MongoDB");
  })
  .catch((error) => {
    console.error("Помилка підключення до бази даних:", error);
  });

const Schema = mongoose.Schema;

const conversationSchema = new Schema({
  members: Array,
});

const conversationRoomSchema = new Schema({
  members: Array,
  roomName: String,
});

const massageSchema = new Schema({
  conversationId: String,
  sender: String,
  text: String,
});

const userSchema = new Schema({
  login: String,
  password: String,
  name: String,
});

const usersModel = mongoose.model("users", userSchema);
const massageModel = mongoose.model("massage", massageSchema);
const conversationModel = mongoose.model("conversations", conversationSchema);
const conversationRoomModel = mongoose.model(
  "conversationsRoom",
  conversationRoomSchema
);

const users = [];

const taskSchema = new mongoose.Schema({
  name: { type: String, required: true },
  date: { type: String, required: true },
  status: { type: String, default: "pending" },
});

const Task = mongoose.model("Task", taskSchema);

io.on("connection", (socket) => {
  console.log(`Новий користувач підключився ${socket.id}`);

  socket.on("move item", (data) => {
    io.emit("move item", data);
  });
  socket.on("new task", async (data) => {
    const { name, date, status, buttonIndex } = data;
    const task = new Task({ name, date, status });
    try {
      await task.save();
      io.emit("new task", { ...data, _id: task._id });
    } catch (err) {
      console.error(err);
    }
  });

  socket.on("chat notification", (receiverId, roomId) => {
    io.emit("chat notification", receiverId, roomId);
  });

  socket.on("chat message", (msg, sender, roomId) => {
    io.emit("chat message", msg, sender, roomId);
  });

  socket.on("disconnect", () => {
    console.log("Користувач відключився");
  });
});

app.get("/get-tasks", async (req, res) => {
  try {
    const tasks = await Task.find();
    res.json(tasks);
  } catch (err) {
    res.status(500).send("Error retrieving tasks from the database");
  }
});
app.put("/move-task", async (req, res) => {
  const _id = req.body._id;
  const status = req.body.status;

  try {
    const task = await Task.findByIdAndUpdate(_id, { status }, { new: true });
    if (!task) {
      return res.status(404).send("Task not found");
    }
    res.json(task);
  } catch (err) {
    res.status(500).send("Error updating task status");
  }
});

app.post("/login", async (req, res) => {
  try {
    const existUser = await usersModel.findOne({ login: req.body.login });

    if (!existUser) {
      return res.status(404).json({ massage: "User not found" });
    }
    if (existUser.password !== req.body.password) {
      return res.status(404).json({ massage: "Incorrect password" });
    }

    res.status(200).json(existUser);
  } catch (err) {
    res.status(500).json(err);
  }
});

app.get("/load-data", (req, res) => {
  pool.query("select * from students", (err, results) => {
    if (err) {
      res.statusCode = 400;
      res.json({
        "Data base error": err,
      });
      return;
    }
    res.json(results);
  });
});

app.put("/edit-data", (req, res) => {
  const newData = req.body;
  const responseData = {};
  const validationStatus = isFormValide(req.body);
  const values = [];
  if (validationStatus.value) {
    console.log(`Дані студента змінено`);
    res.statusCode = 200;

    for (let i in newData) {
      values.push(newData[i]);
    }
    const id = values.shift();
    values.push(+id);
    console.log(values);

    // if (values[1].length > 50) {
    //     res.statusCode = 400;
    //     res.json({
    //         "Massage": "incorect"
    //     })
    //     return
    // }

    pool.query(
      `UPDATE students SET group_name = ?, fname = ?, lname = ?, gender = ?, birthday = ? WHERE id = ?`,
      values,
      (err, results) => {
        if (err) {
          res.statusCode = 400;
          res.json({
            "Data base error": err,
          });
          return;
        }
      }
    );
  } else {
    console.log(`Дані студента НЕКОРЕКТНІ!!!`, validationStatus.report);
    res.statusCode = 400;
    responseData.report = validationStatus.report;
  }

  responseData.statusCode = res.statusCode;
  responseData.data = newData;
  res.json(responseData);
});

app.post("/add-student", (req, res) => {
  const newData = req.body;
  const responseData = {};
  const values = [];
  const validationStatus = isFormValide(req.body);
  if (validationStatus.value) {
    console.log(`Студента додано`);
    res.statusCode = 201;
    for (let i in newData) {
      values.push(newData[i]);
    }
    values.shift();
    pool.query(
      `INSERT INTO students (group_name, fname, lname, gender, birthday) VALUES (?, ?, ?, ?, ?)`,
      values,
      (err, result) => {
        if (err) {
          res.statusCode = 400;
          res.json({
            "Data base error": err,
          });
          return;
        }

        newData.id = result.insertId;
        responseData.data = newData;
        responseData.statusCode = res.statusCode;
        res.json(responseData);
      }
    );
  } else {
    console.log(`Дані студента НЕКОРЕКТНІ!!!`, validationStatus.report);
    res.statusCode = 400;
    responseData.report = validationStatus.report;
    responseData.statusCode = res.statusCode;
    res.json(responseData);
  }
});
app.post("/remove-student", (req, res) => {
  const studentId = req.body.id;
  const responseData = {};

  pool.query(
    `DELETE FROM students WHERE id = ?`,
    [studentId],
    (err, result) => {
      if (err) {
        res.statusCode = 400;
        res.json({
          "Data base error": err,
        });
        return;
      }
      responseData.data = {
        id: studentId,
        massage: "Студента видалено",
      };
      responseData.statusCode = res.statusCode = 200;

      res.json(responseData);
    }
  );
});

function isFormValide(data = {}) {
  const fullAge =
    (new Date() - new Date(data.birthday)) / 1000 / 3600 / 24 / 365;

  const result = {
    value: true,
    report: {
      ageRep: fullAge > 16,
      fnameRep: /^[a-zA-Z]+$/.test(data.fname),
      lnameRep: /^[a-zA-Z]+$/.test(data.lname),
    },
  };
  for (rep in result.report) {
    result.value &= result.report[rep];
  }

  return result;
}

app.get("/", async (req, res) => {
  const users = await usersModel.find({});
  const myAccount = users.find((u) => u._id == req.query.userId);

  if (myAccount) {
    console.log(myAccount);
    res.render("index", {
      loginStatus: "Log out",
      users: users,
      me: myAccount,
    });
  } else {
    res.render("index", { loginStatus: "Log in", me: { name: "User" } });
  }
});
app.get("/tasks", (req, res) => {
  res.render("tasks");
});

app.get("/messenger", (req, res) => {
  res.render("messenger");
});

app.post("/conversation", async (req, res) => {
  const conversations = await conversationModel.find({
    members: { $in: req.body.senderId },
  });
  let exist = false;
  let targetConversation;
  conversations.forEach((i) => {
    if (i.members.includes(req.body.receiverId)) {
      exist = true;
      targetConversation = i;
    }
  });
  if (!exist) {
    const newConversation = new conversationModel({
      members: [req.body.senderId, req.body.receiverId],
    });
    try {
      const result = await newConversation.save();

      res.status(200).json({
        userId: req.body.senderId,
        roomId: result._id,
      });
      return;
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  }
  res.status(200).json({
    userId: req.body.senderId,
    roomId: targetConversation._id,
  });
});

app.get("/conversation-room", async (req, res) => {
  res.status(200).json({
    senderId: req.query.senderId,
    roomId: req.query.roomId,
  });
});
app.post("/conversation-room", async (req, res) => {
  const newConversationRoom = new conversationRoomModel({
    members: [req.body.senderId, ...req.body.receiversId],
    roomName: req.body.roomName,
  });
  try {
    const result = await newConversationRoom.save();

    res.status(200).json({
      userId: req.body.senderId,
      roomId: result._id,
    });
    return;
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.get("/getConversationRooms/:userId", async (req, res) => {
  try {
    const conversationRoom = await conversationRoomModel.find({
      members: { $in: req.params.userId },
    });

    res.status(200).json(conversationRoom);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.get("/room", async (req, res) => {
  try {
    const conversation = await conversationModel.find({
      _id: { $in: req.query.roomId },
    });
    const user = await usersModel.find({
      _id: { $in: req.query.userId },
    });
    const reseiverId = conversation[0].members.filter((i) => i != user[0]._id);

    const reseiver = await usersModel.find({
      _id: { $in: reseiverId[0] },
    });
    const accessStatus = conversation[0].members.includes(user[0]._id);

    if (accessStatus) {
      res.render("room", {
        title: `Chat room with: ${reseiver[0].name}`,
        user: user[0],
        roomId: req.query.roomId,
        loginStatus: "Log out",
      });
    } else {
      res.status(404).json("Page not found");
    }
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});

app.get("/group-room", async (req, res) => {
  try {
    const conversationRoom = await conversationRoomModel.find({
      _id: { $in: req.query.roomId },
    });
    const user = await usersModel.find({
      _id: { $in: req.query.userId },
    });

    const accessStatus = conversationRoom[0].members.includes(user[0]._id);
    if (accessStatus) {
      res.render("group-room", {
        title: `Chat room: ${conversationRoom[0].roomName}`,
        user: user[0],
        roomId: req.query.roomId,
      });
    } else {
      res.status(404).json("Page not found");
    }
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});

app.get("/conversation/:userId", async (req, res) => {
  try {
    const conversations = await conversationModel.find({
      members: { $in: req.params.userId },
    });

    res.status(200).json(conversations);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.post("/massages", async (req, res) => {
  const newMassage = new massageModel(req.body);
  try {
    const massage = await newMassage.save();

    res.status(200).json(massage);
  } catch (err) {
    console.log(massage);
    res.status(500).json(err);
  }
});
app.get("/massages/:conversationId", async (req, res) => {
  try {
    const massages = await massageModel.find({
      conversationId: { $in: req.params.conversationId },
    });

    res.status(200).json(massages);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.get("/group-massages/:conversationId", async (req, res) => {
  try {
    const massages = await massageModel.find({
      conversationId: { $in: req.params.conversationId },
    });

    res.status(200).json(massages);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.get("/getUserByRoom/:roomId", async (req, res) => {
  try {
    const room = await conversationModel.find({
      _id: { $in: req.params.roomId },
    });
    const users = room[0].members;

    res.status(200).json({ users });
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.get("/getUserByGroupRoom/:roomId", async (req, res) => {
  try {
    const room = await conversationRoomModel.find({
      _id: { $in: req.params.roomId },
    });
    const users = room[0].members;

    res.status(200).json({ users });
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.post("/addUserToRoom", async (req, res) => {
  try {
    const room = await conversationRoomModel.findById(req.body.roomId);

    if (!room) {
      return res.status(404).json({ message: "Room not found" });
    }

    // Оновити поле members, додаючи нових користувачів, уникнути дублювання
    const updatedRoom = await conversationRoomModel.findByIdAndUpdate(
      req.body.roomId,
      { $addToSet: { members: { $each: req.body.newUsers } } },
      { new: true, useFindAndModify: false }
    );

    res.status(200).json({ res: updatedRoom });
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});
app.get("/user/:userId", async (req, res) => {
  try {
    const user = await usersModel.findById(req.params.userId);

    res.status(200).json(user);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});

app.get("/users", async (req, res) => {
  try {
    const users = await usersModel.find({});

    res.status(200).json(users);
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});

app.post("/join-chat", (req, res) => {
  const userName = req.body.name;

  if (!users.includes(userName)) {
    users.push(userName);
  }

  res.redirect(`/chat?user=${encodeURIComponent(userName)}`);
});
app.get("/login", (req, res) => {
  res.render("login");
});
app.get("/profile", async (req, res) => {
  try {
    const foundUser = await usersModel.findOne({ _id: req.query.userId });

    if (!foundUser) {
      return res.render("profile", { user: null });
    }

    res.render("profile", { user: foundUser });
  } catch (err) {
    res.status(500).json(err);
  }
});
app.post("/login", async (req, res) => {
  try {
    const existUser = await usersModel.findOne({ login: req.body.login });

    if (!existUser) {
      return res.status(404).json({ massage: "User not found" });
    }
    if (existUser.password !== req.body.password) {
      return res.status(404).json({ massage: "Incorrect password" });
    }

    res.status(200).json(existUser);
  } catch (err) {
    res.status(500).json(err);
  }
});

const PORT = 3300;

server.listen(PORT, () => {
  console.log(`Opened: http://localhost:${PORT}`);
});
