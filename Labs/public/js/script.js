


let id = document.querySelector('.id__input')
let group = document.querySelector('.group__input')
let firstName = document.querySelector('.fname__input')
let lastName = document.querySelector('.lname__input')
let gender = document.querySelector('.gender__input')
let birthday = document.querySelector('.birthday__input')
const mainForm = document.querySelector('form')


window.addEventListener("load", loadDB)



document.querySelector('button.confirm__adding').addEventListener('click', sendData)
document.querySelector('button.confirm__edit').addEventListener('click', editData)

window.addEventListener('click', event => {
    if (event.target.closest('.show__form')) {
        showForm(event.target)
    } else if (event.target.closest(".options__delete")) {
        tryRemove(event.target.closest('tr'))
    }
    if (event.target.closest('.navigation__block.students')) {
        document.querySelector('section.content').style.display = 'block'
        document.querySelector('section.task').style.display = 'none'
        document.querySelector('section.chat').style.display = 'none'
    }
    if (event.target.closest('.navigation__block.task')) {
        document.querySelector('section.content').style.display = 'none'
        document.querySelector('section.task').style.display = 'block'
        document.querySelector('section.chat').style.display = 'none'
    }
    if (event.target.closest('.header__massage')) {
        document.querySelector('section.chat').style.display = 'block'
        document.querySelector('section.content').style.display = 'none'
        document.querySelector('section.task').style.display = 'none'
    }

})

function sendData() {
    if (!isFormFilled()) {
        return
    }
    const newStudent = {}
    const postForm = new FormData(mainForm)
    for (item of postForm) {
        newStudent[item[0]] = item[1]
    }
    console.log("Відправлено на сервер: ", newStudent)

    const jsonData = JSON.stringify(newStudent)
    fetch('/add-student', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: jsonData
    })
        .then(response => response.json())
        .then(data => {
            console.log("Відповідь сервера: ", data);
            if (data.statusCode == 201) {
                hideAddingPage()
                addToTable(data.data)
            } else if (data.report) {
                birthday.closest('.form__item').classList.remove('warning__input')
                firstName.closest('.form__item').classList.remove('warning__input')
                lastName.closest('.form__item').classList.remove('warning__input')

                if (!data.report["ageRep"]) {
                    birthday.closest('.form__item').classList.add('warning__input')
                }
                if (!data.report["fnameRep"]) {
                    firstName.closest('.form__item').classList.add('warning__input')
                }
                if (!data.report["lnameRep"]) {
                    lastName.closest('.form__item').classList.add('warning__input')
                }

                console.error("Некоректна форма")
                return
            }
        })
        .catch(error => console.error('Помилка:', error));
}

function editData() {
    if (!isFormFilled()) {
        return
    }
    const newStudent = {}
    const postForm = new FormData(mainForm)
    for (item of postForm) {
        newStudent[item[0]] = item[1]
    }
    console.log("Відправлено на сервер: ", newStudent)

    const jsonData = JSON.stringify(newStudent)
    fetch('/edit-data', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: jsonData
    })
        .then(response => response.json())
        .then(data => {
            console.log("Відповідь сервера: ", data);
            if (data.statusCode == 200) {
                let targerRow
                document.querySelectorAll(`.table__rows`).forEach(row => {
                    if (row.dataset.index == id.value) {
                        targerRow = row
                    }
                })


                targerRow.querySelector('.table__group').textContent = group.value
                targerRow.querySelector('.table__name').textContent = firstName.value + " " + lastName.value
                targerRow.querySelector('.table__gender').textContent = gender.value
                targerRow.querySelector('.table__birthday').textContent = birthday.value


                hideAddingPage()
            } else if (data.report) {
                birthday.closest('.form__item').classList.remove('warning__input')
                firstName.closest('.form__item').classList.remove('warning__input')
                lastName.closest('.form__item').classList.remove('warning__input')

                if (!data.report["ageRep"]) {
                    birthday.closest('.form__item').classList.add('warning__input')
                }
                if (!data.report["fnameRep"]) {
                    firstName.closest('.form__item').classList.add('warning__input')
                }
                if (!data.report["lnameRep"]) {
                    lastName.closest('.form__item').classList.add('warning__input')
                }

                console.error("Некоректна форма")
                return
            }
        })
        .catch(error => console.error('Помилка:', error));
}

function isFormFilled() {
    const inputs = document.querySelectorAll('.input')
    for (item of inputs) {
        if (item.value == '') {
            document.querySelectorAll('.form__item').
                forEach(i => {
                    if (i.classList.contains('warning__input')) {
                        i.classList.remove('warning__input')
                    }
                    setTimeout(() => {
                        if (i.querySelector('.input').value == '') {
                            i.classList.add('warning__input')
                        }
                    }, 0)
                })
            return
        }
    }
    return true
}

function clearForm() {
    group.value = ''
    firstName.value = ''
    lastName.value = ''
    gender.value = ''
    birthday.value = ''
}

function makeFormDefoult() {
    document.querySelectorAll('.form__item').forEach(parent => {
        parent.classList.remove('warning__input')
    })
    clearForm()
}

function showForm(target) {
    document.querySelector('.adding__page').style.display = 'flex'
    const title = document.querySelector('.chaning__title')
    if (target.closest('button.add__student')) {
        document.querySelector('button.confirm__edit').style.display = 'none'
        document.querySelector('button.confirm__adding').style.display = 'block'
        title.textContent = 'Add Student'
    } else if (target.closest('button.options__edit')) {
        document.querySelector('button.confirm__adding').style.display = 'none'
        document.querySelector('button.confirm__edit').style.display = 'block'
        title.textContent = 'Edit Student'

        const targetRow = target.closest('tr')

        id.value = targetRow.dataset.index
        group.value = targetRow.querySelector('.table__group').textContent
        firstName.value = targetRow.querySelector('.table__name').textContent.split(' ')[0]
        lastName.value = targetRow.querySelector('.table__name').textContent.split(' ')[1]
        gender.value = targetRow.querySelector('.table__gender').textContent
        birthday.value = targetRow.querySelector('.table__birthday').textContent
    }
}

function hideAddingPage() {
    document.querySelector('.adding__page').style.display = 'none'
    makeFormDefoult()
}

function addToTable(student) {

    const tr = document.createElement('tr')
    tr.className = 'table__rows'
    tr.dataset.index = student.id
    tr.innerHTML =
        `<td class="table__content table__checkbox">
                <input type="checkbox" name="checkbox${student['id']}" class="checkbox">
            </td>
            <td class="table__content table__group">${student.group_name}</td>
            <td class="table__content table__name">${student.fname + " " + student.lname}</td>
            <td class="table__content table__gender">${student.gender}</td>
            <td class="table__content table__birthday">${student.birthday}</td>
            <td class="table__content table__status">
                <div class="circle">
                </div>
            </td>
            <td class="table__content options">
                <button class="options__edit show__form">
                    <img src="./img/pencil.svg">
                </button>
                <button class="options__delete">
                    <img src="./img/cross.png">
                </button>
                <button class="options__hide-showe">
                    !
                </button>
            </td>`
    document.querySelector('.table__body').appendChild(tr)
    selectStudents()

}

function tryRemove(element) {
    const userName = element.querySelectorAll('.table__content')[2].textContent

    const warning = document.querySelector(".warning__page")
    warning.style.display = 'flex'
    warning.querySelector('.warning__main').textContent = `Are you sure you want to delete ${userName}?`





    warning.querySelector('.confirm__warning').onclick = async () => {
        const jsonData = JSON.stringify({ id: +element.dataset.index })
        fetch('/remove-student', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: jsonData
        })
            .then(response => response.json())
            .then(data => {
                if (data.statusCode == 200) {
                    element.remove()
                }
                warning.style.display = 'none'
            })
            .catch(error => console.error('Помилка:', error));


    }
}


//   DB
// ===================================================================
function loadDB() {
    fetch('/load-data')
        .then(response => response.json())
        .then(data => {
            data.forEach(item => {
                addToTable(item)
            })
        })
        .catch(error => console.error('Помилка:', error))
}
// ===================================================================
function selectStudents() {
    const checkboxes = document.querySelectorAll('.checkbox')
    checkboxes.forEach((element, index) => {
        element.onchange = () => {
            if (index != 0 && !element.checked) {
                checkboxes[0].checked = element.checked
            }
        }
    })
    checkboxes[0].onchange = () => {
        checkboxes.forEach(element => {
            element.checked = checkboxes[0].checked
        })
    }
}
//Hiden management
window.addEventListener("click", event => {
    if (window.innerWidth <= 650) {
        if (event.target.closest('.options__hide-showe')) {
            document.querySelectorAll('.table__rows').forEach(item => {
                item.querySelector('.options__edit').style.display = 'none';
                item.querySelector('.options__delete').style.display = 'none';
            })

            const tr = event.target.closest('.table__rows')
            tr.querySelector('.options__edit').style.display = 'block'
            tr.querySelector('.options__delete').style.display = 'block'

        }
        if (!event.target.closest('.options')) {
            document.querySelectorAll('.table__rows').forEach(item => {
                item.querySelector('.options__edit').style.display = 'none';
                item.querySelector('.options__delete').style.display = 'none';
            })
        }
    }
})

// Mobile-screen styles
window.addEventListener("resize", controleStyleDespiteToSize)
window.addEventListener("load", controleStyleDespiteToSize)

function controleStyleDespiteToSize() {
    const gender = document.querySelector('.table__title-gender')
    const status = document.querySelector('.table__title-status')
    const options = document.querySelector('.table__title-options')
    gender.style.fontWeight =
        status.style.fontWeight =
        options.style.fontWeight = '400'

    if (window.innerWidth <= 650) {
        gender.textContent = 'G'
        status.textContent = 'S'
        options.textContent = '...'

    } else {
        gender.textContent = 'Gender'
        status.textContent = 'Status'
        options.textContent = 'Options'
    }
}