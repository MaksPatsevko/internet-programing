// const socket = io();

const buttons = document.querySelectorAll('.add-task__button');
const newTaskButtons = document.querySelectorAll('.create-task__button');


async function loadTasks() {
    try {
        const response = await fetch('/get-tasks');
        const tasks = await response.json();

        // Очистити всі списки перед вставкою нових завдань
        document.querySelectorAll('.list-group').forEach(list => {
            list.innerHTML = '';
        });

        tasks.forEach(task => {
            const taskHTML = `
            <li data-index="${task._id}" class="list-group-item py-4" >
                <span>${task.name}</span>
                <span>${task.date}</span>
            </li>`;

            const statusUndex = {
                "To do": 0,
                "In progress": 1,
                "Done": 2
            }
            document.querySelectorAll('.list-group').forEach((ul, index) => {
                const isEmpty = ul.children.length === 0;
                document.querySelectorAll('.empty__massage')[index].style.display = isEmpty ? 'block' : 'none';
            });
            // Вставити завдання у відповідний список (тут передбачено, що всі завдання вставляються в перший список)
            document.querySelectorAll('.list-group')[statusUndex[task.status]].insertAdjacentHTML('beforeend', taskHTML);
        });

    } catch (err) {
        console.error('Failed to load tasks', err);
    }
}
loadTasks()
newTaskButtons.forEach((button, buttonIndex) => {
    button.addEventListener('click', () => {
        const name = document.getElementById('new-task__name').value;
        const date = document.getElementById('new-task__date').value;

        if (name != '' && date != '') {
            const status = button.parentElement.querySelector('.list-group').closest('.parent-node').querySelector('h3').textContent

            socket.emit('new task', {
                name,
                date,
                status,
                buttonIndex
            });
        } else {
            alert('empty input')
        }
    });
});

socket.on('new task', (data) => {
    const { name, date, status, buttonIndex, _id } = data;
    const button = newTaskButtons[buttonIndex];
    const listGroup = button.parentElement.querySelector('.list-group');

    const taskHTML = `
    <li data-index="${_id}" class="list-group-item py-4">
        <span>${name}</span>
        <span>${date}</span>
    </li>`;
    listGroup.insertAdjacentHTML("beforeend", taskHTML);
});


buttons.forEach((button, buttonIndex) => {
    button.addEventListener('click', () => {
        button.classList.add('clicked', 'bg-primary');
        button.classList.remove('bg-white');
        button.textContent = 'Select task to add';

        setTimeout(() => {
            const bodyClickHandler = (event) => {
                button.classList.remove('clicked', 'bg-primary');
                button.classList.add('bg-white');
                button.textContent = '+ Add task';

                const target = event.target.closest('.list-group-item');
                if (target) {
                    const listGroup = button.parentElement.querySelector('.list-group');
                    const targetHTML = target.outerHTML;
                    const fromList = target.parentElement;
                    //target.remove();

                    // Перевірка порожніх списків
                    document.querySelectorAll('.list-group').forEach((ul, index) => {
                        const isEmpty = ul.children.length === 0;
                        document.querySelectorAll('.empty__massage')[index].style.display = isEmpty ? 'block' : 'none';
                    });

                    // Відправити подію через socket.io
                    socket.emit('move item', {
                        buttonIndex,
                        targetHTML,
                        fromListIndex: Array.from(document.querySelectorAll('.list-group')).indexOf(fromList),
                        _id: target.dataset.index
                    });
                }

                // Видалення слухача подій після його спрацювання
                document.querySelector('body').removeEventListener('click', bodyClickHandler);
            };

            document.querySelector('body').addEventListener('click', bodyClickHandler, { capture: false, once: true });
        }, 0);
    });
});

// Отримання подій від інших клієнтів
socket.on('move item', async (data) => {
    const { buttonIndex, targetHTML, fromListIndex, _id } = data;
    const button = buttons[buttonIndex];
    const listGroup = button.parentElement.querySelector('.list-group');

    // Вставити HTML елемента в список
    listGroup.insertAdjacentHTML('beforeend', targetHTML);
    const newStatus = listGroup.closest('.parent-node').querySelector('h3').textContent
    fetch(`/move-task`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            _id: _id,
            status: newStatus
        })
    });
    // Видалити елемент з попереднього списку
    const fromListGroup = document.querySelectorAll('.list-group')[fromListIndex];
    const target = Array.from(fromListGroup.children).find(child => child.outerHTML === targetHTML);
    if (target) {
        target.remove();
    }

    // Перевірка порожніх списків
    document.querySelectorAll('.list-group').forEach((ul, index) => {
        const isEmpty = ul.children.length === 0;
        document.querySelectorAll('.empty__massage')[index].style.display = isEmpty ? 'block' : 'none';
    });
});
