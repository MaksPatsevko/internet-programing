const urlParams = new URLSearchParams(window.location.search);


const userId = urlParams.get('userId')
console.log(userId)

const socket = io();

fetch('/getConversationRooms/' + userId)
    .then(res => res.json())
    .then(data => {
        const ul = document.querySelector('.groups-list')

        data.forEach(i => {
            const li = document.createElement("li")
            li.textContent = i.roomName
            const button = document.createElement("button")
            button.textContent = 'Join'


            li.appendChild(button)
            li.dataset.index = i._id

            button.addEventListener('click', () => {
                // window.location.href = `conversation-room?senderId=${userId}&roomId=${i._id}`;
                window.location.href = `group-room?userId=${userId}&roomId=${i._id}`;

            })

            ul.appendChild(li)
        })
    })


document.getElementById('new__chat').addEventListener('click', () => {
    const checkboxes = document.querySelectorAll('input[name="user"]:checked');
    const roomNameInput = document.querySelector('input[name="room__name"]');
    const receivers = Array.from(checkboxes).map(i => i.parentNode.dataset.index)
    fetch("/conversation-room", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            senderId: userId,
            receiversId: receivers,
            roomName: roomNameInput.value
        })
    })
    document.querySelector('.form').style.display = 'none'
})



document.querySelectorAll('.start__conversation').forEach(i => {
    const li = i.closest('li')
    i.addEventListener('click', () => {
        fetch(`/conversation`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                senderId: userId,
                receiverId: li.dataset.index
            })
        })
            .then(response => response.json())
            .then(data => {
                window.location.href = `room?userId=${data.userId}&roomId=${data.roomId}`;
            })
    })
})

document.getElementById('create__room').addEventListener('click', () => {

    document.querySelector('.form').style.display = 'flex'
})

socket.on('chat notification', (receiverId, roomId) => {
    if (receiverId == userId) {
        // alert(`New massage in ${roomId}`)
        const bell = document.getElementById('bell__img')

        if (bell.className) {
            bell.className = ''
        }
        bell.className = 'bell__img'
        setTimeout(() => {
            bell.className = ''
        }, 2000);
    }
})