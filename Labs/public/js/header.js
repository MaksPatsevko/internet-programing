document.querySelector('.header__account').addEventListener('click', () => {
    document.querySelector('.modal__account').style.display = 'block'
})
document.addEventListener('click', (event) => {
    if (!event.target.closest('.header__account'))
        document.querySelector('.modal__account').style.display = 'none'
})
const logButton = document.querySelectorAll('.modal__account-block')[1]
const loginStatus = logButton.dataset.login
logButton.textContent = loginStatus
logButton.addEventListener('click', (event) => {
    event.preventDefault()
    console.log(event.target.closest('.modal__account-block'))
    if (event.target.closest('.modal__account-block').dataset.login == "Log in")
        document.querySelector('.login__page').style.display = 'flex'
    else
        window.location.href = `/`;
})



const loginInput = document.getElementById('loginUsername')
const passwordInput = document.getElementById('loginPassword')
document.getElementById('try__login').addEventListener('click', async () => {
    fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "login": loginInput.value,
            "password": passwordInput.value
        })
    })
        .then(response => response.json())
        .then(data => {
            // console.log(data._id)
            window.location.href = `/?userId=${data._id}`;
        })

})
// document.querySelector('.header__massage').addEventListener('click', () => {
//     document.querySelector('.modal__masage').style.display = 'block'
// })
// document.addEventListener('click', (event) => {
//     if (!event.target.closest('.header__massage'))
//         document.querySelector('.modal__masage').style.display = 'none'
// })